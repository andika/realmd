# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Adolfo Jayme Barrientos, 2013
# Adolfo Jayme Barrientos, 2013
# Daniel Mustieles <daniel.mustieles@gmail.com>, 2013
msgid ""
msgstr ""
"Project-Id-Version: realmd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-19 11:43+0200\n"
"PO-Revision-Date: 2015-10-20 19:12+0000\n"
"Last-Translator: Stef Walter <stefw@gnome.org>\n"
"Language-Team: Spanish (http://www.transifex.com/freedesktop/realmd/language/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../service/org.freedesktop.realmd.policy.in.h:1
msgid "Discover realm"
msgstr "Descubrir reino"

#: ../service/org.freedesktop.realmd.policy.in.h:2
msgid "Authentication is required to discover a kerberos realm"
msgstr "Se requiere autenticación para descubrir un reino de Kerberos"

#: ../service/org.freedesktop.realmd.policy.in.h:3
msgid "Join machine to realm"
msgstr "Unir equipo al reino"

#: ../service/org.freedesktop.realmd.policy.in.h:4
msgid "Authentication is required to join this machine to a realm or domain"
msgstr "Se requiere autenticación para unir este equipo a un reino o a un dominio"

#: ../service/org.freedesktop.realmd.policy.in.h:5
msgid "Remove machine from realm"
msgstr "Quitar equipo del reino"

#: ../service/org.freedesktop.realmd.policy.in.h:6
msgid ""
"Authentication is required to remove this computer from a realm or domain."
msgstr "Se requiere autenticación para quitar este equipo de un reino o de un dominio."

#: ../service/org.freedesktop.realmd.policy.in.h:7
msgid "Change login policy"
msgstr "Cambiar política de inicio de sesión"

#: ../service/org.freedesktop.realmd.policy.in.h:8
msgid ""
"Authentication is required to change the policy of who can log in on this "
"computer."
msgstr "Para cambiar la política sobre quién puede iniciar sesión en este equipo, necesita autenticarse."

#: ../service/realm-command.c:347
#, c-format
msgid "Process was terminated with signal: %d"
msgstr "El proceso finalizó con la señal: %d"

#: ../service/realm-command.c:396 ../service/realm-ldap.c:349
#, c-format
msgid "The operation was cancelled"
msgstr "Se canceló la operación"

#: ../service/realm-command.c:573
#, c-format
msgid "Configured command not found: %s"
msgstr "No se encontró la orden configurada: %s"

#: ../service/realm-command.c:578
#, c-format
msgid "Skipped command: %s"
msgstr "Orden omitida: %s"

#: ../service/realm-command.c:584
#, c-format
msgid "Configured command invalid: %s"
msgstr "La orden configurada no es válida: %s"

#: ../service/realm-disco-mscldap.c:198
#, c-format
msgid "Received invalid or unsupported Netlogon data from server"
msgstr "Se han recibido datos de Netlogon no válidos o no soportados del servidor"

#: ../service/realm-disco-mscldap.c:346
msgid "LDAP on this system does not support UDP connections"
msgstr "En este sistema, LDAP no soporta conexiones UDP"

#: ../service/realm-example.c:82 ../service/realm-samba.c:247
#: ../service/realm-sssd-ad.c:309 ../service/realm-sssd-ipa.c:318
#, c-format
msgid "Unsupported or unknown membership software '%s'"
msgstr "El software de membresía «%s» es desconocido o no es compatible"

#: ../service/realm-example.c:170 ../service/realm-samba.c:282
msgid "Already joined to a domain"
msgstr "Ya se unió a un dominio"

#: ../service/realm-example.c:178 ../service/realm-example.c:271
msgid "Admin name or password is not valid"
msgstr "El nombre o la contraseña del administrador no es válido"

#: ../service/realm-example.c:242 ../service/realm-samba.c:422
#: ../service/realm-sssd-ad.c:514
msgid "Not currently joined to this domain"
msgstr "Actualmente no se ha unido a este dominio"

#: ../service/realm-example.c:301
msgid "Need credentials for leaving this domain"
msgstr "Necesita credenciales para abandonar este dominio"

#: ../service/realm-ini-config.c:736
#, c-format
msgid "Couldn't write out config: %s"
msgstr "No se pudo escribir la configuración: %s"

#: ../service/realm-invocation.c:536
msgid "Not authorized to perform this action"
msgstr "No tiene autorización para realizar esta acción"

#: ../service/realm-kerberos.c:130 ../service/realm-kerberos.c:208
#: ../service/realm-provider.c:156
msgid "Operation was cancelled."
msgstr "Se canceló la operación."

#: ../service/realm-kerberos.c:135
msgid "Failed to enroll machine in realm. See diagnostics."
msgstr "Falló al unir el equipo al reino. Consulte los diagnósticos."

#: ../service/realm-kerberos.c:213
msgid "Failed to unenroll machine from domain. See diagnostics."
msgstr "Falló al sacar el equipo del dominio. Consulte los diagnósticos."

#: ../service/realm-kerberos.c:272
msgid "Joining this realm without credentials is not supported"
msgstr "No está soportado el unirse a este reino sin credenciales"

#: ../service/realm-kerberos.c:273
msgid "Leaving this realm without credentials is not supported"
msgstr "No está soportado el salir de este reino sin credenciales"

#: ../service/realm-kerberos.c:276
msgid "Joining this realm using a credential cache is not supported"
msgstr "No está soportado el unirse a este reino usando una caché de credenciales"

#: ../service/realm-kerberos.c:277
msgid "Leaving this realm using a credential cache is not supported"
msgstr "No está soportado el salir de este reino usando una caché de credenciales"

#: ../service/realm-kerberos.c:280
msgid "Joining this realm using a secret is not supported"
msgstr "No está soportado el unirse a este reino usando un secreto"

#: ../service/realm-kerberos.c:281
msgid "Unenrolling this realm using a secret is not supported"
msgstr "No está soportado el salir de este reino usando un secreto"

#: ../service/realm-kerberos.c:284
msgid "Enrolling this realm using a password is not supported"
msgstr "No está soportado el unirse a este reino usando una contraseña"

#: ../service/realm-kerberos.c:285
msgid "Unenrolling this realm using a password is not supported"
msgstr "No está soportado el salir de este reino usando una contraseña"

#: ../service/realm-kerberos.c:311
msgid "Joining this realm is not supported"
msgstr "No está soportado el unirse a este reino"

#: ../service/realm-kerberos.c:312
msgid "Leaving this realm is not supported"
msgstr "No está soportado el salir de este reino"

#: ../service/realm-kerberos.c:332 ../service/realm-kerberos.c:504
msgid "Already running another action"
msgstr "Ya se está ejecutando otra acción"

#: ../service/realm-kerberos.c:375
#, c-format
msgid "Already joined to another domain: %s"
msgstr "Ya se ha unido a otro dominio: %s"

#: ../service/realm-kerberos.c:446
msgid "Failed to change permitted logins. See diagnostics."
msgstr "Falló al cambiar los inicios de sesión permitidos. Consulte los diagnósticos."

#: ../service/realm-kerberos.c:747
#, c-format
msgid "The realm does not allow specifying logins"
msgstr "El reino no permite los inicios de sesión específicos"

#: ../service/realm-kerberos.c:755
#, c-format
msgid "Invalid login argument%s%s%s does not match the login format."
msgstr "El argumento%s%s%s no válido no coincide con el formato de inicio de sesión."

#: ../service/realm-packages.c:389
#, c-format
msgid "The following packages are not available for installation: %s"
msgstr "Los siguientes paquetes no están disponibles para instalarlos: %s"

#: ../service/realm-packages.c:471 ../service/realm-packages.c:508
#, c-format
msgid "Necessary packages are not installed: %s"
msgstr "Paquetes necesarios no instalados: %s"

#. * Various people have been worried by installing packages
#. * quietly, so notify about what's going on.
#. *
#. * In reality *configuring* and *starting* a daemon is far
#. * more worrisome than the installation. It's realmd's job
#. * to configure, enable and start stuff. So if you're properly
#. * worried, remove realmd and do stuff manually.
#: ../service/realm-packages.c:477 ../tools/realm-client.c:143
msgid "Installing necessary packages"
msgstr "Instalando los paquetes necesarios"

#: ../service/realm-provider.c:161
msgid "Failed to discover realm. See diagnostics."
msgstr "Falló al descubrir reino. Consulte los diagnósticos."

#: ../service/realm-samba.c:467
#, c-format
msgid "Not joined to this domain"
msgstr "No se ha unido a este dominio"

#: ../service/realm-samba.c:477 ../service/realm-samba.c:519
#, c-format
msgid "The Samba provider cannot restrict permitted logins."
msgstr "El proveedor Samba no puede restringir los inicios de sesión permitidos."

#: ../service/realm-sssd.c:130
#, c-format
msgid "Invalid login argument '%s' contains unsupported characters."
msgstr "El argumento de inicio de sesión «%s» no válido contiene caracteres no soportados"

#: ../service/realm-sssd-ad.c:126 ../service/realm-sssd-ipa.c:124
#, c-format
msgid "Enabling SSSD in nsswitch.conf and PAM failed."
msgstr "Falló al activar SSSD en nsswitch.conf y PAM."

#: ../service/realm-sssd-ad.c:240
#, c-format
msgid "Unable to automatically join the domain"
msgstr "No se pudo unir automáticamente al dominio"

#: ../service/realm-sssd-ad.c:326
#, c-format
msgid ""
"Joining a domain with a one time password is only supported with the '%s' "
"membership software"
msgstr "Unirse a un dominio con una contraseña de un solo uso solo está soportado con el software del miembro «%s»"

#: ../service/realm-sssd-ad.c:340
#, c-format
msgid ""
"Joining a domain with a user password is only supported with the '%s' "
"membership software"
msgstr "Unirse a un dominio con una contraseña de usuario solo está soportado con el software del miembro «%s»"

#: ../service/realm-sssd-ad.c:356
#, c-format
msgid "Unsupported credentials for joining a domain"
msgstr "No se admiten las credenciales para unirse al dominio"

#: ../service/realm-sssd-ad.c:398 ../service/realm-sssd-ipa.c:322
#: ../tools/realm-join.c:236
msgid "Already joined to this domain"
msgstr "Ya se ha unido a este dominio"

#: ../service/realm-sssd-ad.c:402 ../service/realm-sssd-ipa.c:326
msgid "A domain with this name is already configured"
msgstr "Ya está configurado un dominio con este nombre"

#: ../service/realm-sssd-config.c:145
#, c-format
msgid "Already have domain %s in sssd.conf config file"
msgstr "El dominio %s ya está en el archivo de configuración sssd.conf"

#: ../service/realm-sssd-config.c:190
#, c-format
msgid "Don't have domain %s in sssd.conf config file"
msgstr "No tiene al dominio %s en el archivo de configuración sssd.conf"

#: ../service/realm-sssd-ipa.c:312
msgid "The computer-ou argument is not supported when joining an IPA domain."
msgstr "El argumento del equipo no está soportado al unirse a un dominio IPA."

#: ../service/realm-sssd-ipa.c:480
msgid "Not currently joined to this realm"
msgstr "Actualmente no se ha unido a este dominio"

#: ../tools/realm.c:40
msgid "Discover available realm"
msgstr "Descubrir reino disponible"

#: ../tools/realm.c:41
msgid "Enroll this machine in a realm"
msgstr "Unir este equipo a un reino"

#: ../tools/realm.c:42
msgid "Unenroll this machine from a realm"
msgstr "Sacar este equipo de un reino"

#: ../tools/realm.c:43
msgid "List known realms"
msgstr "Listar los reinos conocidos"

#: ../tools/realm.c:44
msgid "Permit user logins"
msgstr "Permitir inicios de sesión de usuario"

#: ../tools/realm.c:45
msgid "Deny user logins"
msgstr "Denegar inicios de sesión de usuario"

#: ../tools/realm.c:184
#, c-format
msgid "Invalid value for %s option: %s"
msgstr ""

#: ../tools/realm.c:216
msgid "Install mode to a specific prefix"
msgstr "Instalar modo en un prefijo específico"

#: ../tools/realm.c:217
msgid "Verbose output"
msgstr "Salida detallada"

#: ../tools/realm.c:218
msgid "Do not prompt for input"
msgstr "No preguntar"

#: ../tools/realm-client.c:193 ../tools/realm-client.c:203
msgid "Couldn't connect to realm service"
msgstr "No se pudo conectar al servicio del reino"

#: ../tools/realm-client.c:232
msgid "Couldn't load the realm service"
msgstr "No se pudo cargar el servicio del reino"

#: ../tools/realm-client.c:249
msgid "Couldn't connect to system bus"
msgstr "No se pudo conectar al bus del sistema"

#: ../tools/realm-client.c:279
#, c-format
msgid "Couldn't create socket pair: %s"
msgstr "No se pudo crear el par del socket: %s"

#: ../tools/realm-client.c:287 ../tools/realm-client.c:319
msgid "Couldn't create socket"
msgstr "No se pudo crear el socket"

#: ../tools/realm-client.c:300
msgid "Couldn't run realmd"
msgstr "No se pudo ejecutar realmd"

#: ../tools/realm-client.c:565
#, c-format
msgid "Couldn't create runtime directory: %s: %s"
msgstr "No se pudo crear la carpeta en tiempo de ejecución: %s: %s"

#: ../tools/realm-client.c:575
#, c-format
msgid "Couldn't create credential cache file: %s: %s"
msgstr "No se pudo crear el archivo de la caché de credenciales: %s: %s"

#: ../tools/realm-client.c:585
msgid "Couldn't resolve credential cache"
msgstr "No se pudo resolver la caché de credenciales"

#: ../tools/realm-client.c:675
#, c-format
msgid "Invalid password for %s"
msgstr "Contraseña no válida para %s"

#: ../tools/realm-client.c:679
#, c-format
msgid "Couldn't authenticate as %s"
msgstr "No se pudo autenticar como %s"

#: ../tools/realm-client.c:704
#, c-format
msgid "Couldn't parse user name: %s"
msgstr "No se pudo analizar el nombre de usuario: %s"

#: ../tools/realm-client.c:728
msgid "Couldn't read credential cache"
msgstr "No se pudo leer la caché de la credencial"

#: ../tools/realm-client.c:754 ../tools/realm-client.c:961
msgid "Couldn't initialize kerberos"
msgstr "No se pudo inicializar Kerberos"

#: ../tools/realm-client.c:817
#, c-format
msgid "Cannot prompt for a password when running in unattended mode"
msgstr "No se puede solicitar una contraseña al ejecutarse en modo desatendido"

#: ../tools/realm-client.c:821
#, c-format
msgid "Password for %s: "
msgstr "Contraseña para %s:"

#: ../tools/realm-client.c:839
#, c-format
msgid "Couldn't prompt for password: %s"
msgstr "No se pudo solicitar la contraseña: %s"

#: ../tools/realm-client.c:874
#, c-format
msgid "Realm does not support membership using a password"
msgstr "El reino no soporta que los miembros usen una contraseña"

#: ../tools/realm-client.c:913
#, c-format
msgid "Realm does not support membership using a one time password"
msgstr "El reino no soporta que los miembros usen una contraseña de un solo uso"

#: ../tools/realm-client.c:981
msgid "Couldn't select kerberos credentials"
msgstr "No se pudieron seleccionar las credencias de Kerberos"

#: ../tools/realm-client.c:995
msgid "Couldn't read kerberos credentials"
msgstr "No se pudieron leer las credencias de Kerberos"

#: ../tools/realm-client.c:1045
#, c-format
msgid "Realm does not support automatic membership"
msgstr "El reino no soporta los miembros automáticos"

#: ../tools/realm-discover.c:135
msgid "Couldn't discover realms"
msgstr "No se pudieron descubrir los reinos"

#: ../tools/realm-discover.c:155
msgid "No default realm discovered"
msgstr "No se ha descubierto ningún reino predeterminado"

#: ../tools/realm-discover.c:157
#, c-format
msgid "No such realm found: %s"
msgstr "No se ha encontrado el reino: %s"

#: ../tools/realm-discover.c:181
msgid "Show all discovered realms"
msgstr "Mostrar todos los reinos descubiertos"

#: ../tools/realm-discover.c:182 ../tools/realm-discover.c:273
msgid "Show only the names"
msgstr "Solo mostrar los nombres"

#: ../tools/realm-discover.c:183 ../tools/realm-join.c:287
#: ../tools/realm-leave.c:266
msgid "Use specific client software"
msgstr "Utilizar un software cliente específico"

#: ../tools/realm-discover.c:184 ../tools/realm-join.c:291
msgid "Use specific membership software"
msgstr "Usar software de membresía específico"

#: ../tools/realm-discover.c:185 ../tools/realm-join.c:289
#: ../tools/realm-leave.c:269
msgid "Use specific server software"
msgstr "Usar software de servidor específico"

#: ../tools/realm-discover.c:272
msgid "Show all realms"
msgstr "Mostrar todos los reinos"

#: ../tools/realm-join.c:92 ../tools/realm-join.c:132
#: ../tools/realm-join.c:163
msgid "Couldn't join realm"
msgstr "No se pudo unir al reino"

#: ../tools/realm-join.c:227
msgid "Cannot join this realm"
msgstr "No se puede unir a este reino"

#: ../tools/realm-join.c:229
msgid "No such realm found"
msgstr "No se ha encontrado el reino"

#: ../tools/realm-join.c:283
msgid "User name to use for enrollment"
msgstr "Nombre de usuario que usar para unirse"

#: ../tools/realm-join.c:285
msgid "Computer OU DN to join"
msgstr "OU DN del equipo que unir"

#: ../tools/realm-join.c:293
msgid "Join automatically without a password"
msgstr "Unirse automáticamente sin una contraseña"

#: ../tools/realm-join.c:295
msgid "Join using a preset one time password"
msgstr "Unirse con una contraseña preestablecida de un solo uso"

#: ../tools/realm-join.c:297
msgid "Turn off automatic id mapping"
msgstr ""

#: ../tools/realm-join.c:299
msgid "Set the user principal for the computer account"
msgstr "Establecer el usuario principal para la cuenta del equipo"

#: ../tools/realm-join.c:319
msgid "Specify one realm to join"
msgstr "Especificar un reino al que unirse"

#: ../tools/realm-join.c:324
msgid ""
"The --no-password argument cannot be used with --one-time-password or --user"
msgstr "No se puede usar el argumento --no-password con --one-time-password ni con --user"

#: ../tools/realm-join.c:329
msgid "The --one-time-password argument cannot be used with --user"
msgstr "No se puede usar el argumento--one-time-password con --user"

#: ../tools/realm-leave.c:181 ../tools/realm-leave.c:208
msgid "Couldn't leave realm"
msgstr "No se pudo abandonar el reino"

#: ../tools/realm-leave.c:267
msgid "Remove computer from realm"
msgstr "Quitar equipo del reino"

#: ../tools/realm-leave.c:270
msgid "User name to use for removal"
msgstr "Nombre de usuario que usar para quitar"

#: ../tools/realm-logins.c:129 ../tools/realm-logins.c:175
msgid "Couldn't change permitted logins"
msgstr "No se pudieron cambiar los inicios de sesión permitidos"

#: ../tools/realm-logins.c:200
msgid "Permit any realm account login"
msgstr "Permitir el inicio de sesión a cualquier cuenta del reino"

#: ../tools/realm-logins.c:200
msgid "Deny any realm account login"
msgstr "Denegar el inicio de sesión a cualquier cuenta del reino"

#: ../tools/realm-logins.c:202
msgid "Withdraw permit for a realm account to login"
msgstr "Quitar permiso para iniciar sesión a una cuenta del reino"

#: ../tools/realm-logins.c:204
msgid "Treat names as groups which to permit"
msgstr "Tratar nombres como grupos a los que permitir"

#: ../tools/realm-logins.c:205
msgid "Realm to permit/deny logins for"
msgstr "Reino en el que permitir/denegar los inicios de sesión"

#: ../tools/realm-logins.c:219
msgid "No logins should be specified with -a or --all"
msgstr "No se deben especificar inicios de sesión con -a ni con --all"

#: ../tools/realm-logins.c:222
msgid "The --withdraw or -x arguments cannot be used when denying logins"
msgstr "No se pueden usar los argumentos --withdraw ni -x al denegar inicios de sesión"

#: ../tools/realm-logins.c:225
msgid "Specific logins must be specified with --withdraw"
msgstr "Los inicios de sesión específicos se deben especificar con --withdraw"

#: ../tools/realm-logins.c:228
msgid "Groups may not be specified with -a or --all"
msgstr "No se pueden especificar los grupos con -a ni con --all"

#: ../tools/realm-logins.c:235
msgid "Use --all to deny all logins"
msgstr "Usar --all para denegar todos los inicios de sesión"

#: ../tools/realm-logins.c:237
msgid "Specify specific users to add or remove from the permitted list"
msgstr "Especificar los usuarios que añadir o quitar de la lista de permitidos"

#: ../tools/realm-logins.c:241
msgid ""
"Specifying deny without --all is deprecated. Use realm permit --withdraw"
msgstr "Especificar la denegación con --all está obsoleto. Use  realm permit --withdraw"
